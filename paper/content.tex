\begin{abstract}
   Stromatolites are colonies of bacteria that self-assemble into rock
   formations in tidal salt flats. Each stromatolite can make independent
   decisions, while maintaining communication with the colony. The workload is
   shared among all colony individuals. Stromatolites breed rapidly, and quickly
   develop resistance to antibiotics and other threats by developing new genes.
   I plan to incorporate these principles into a simulated data filtering
   system. The system is composed by autonomous agents acting like bacteria.
   Workload i.e., incoming data, will be spread evenly through the network. And
   in a process mimicking natural selection, desirable data will be quickly
   distributed to incoming knowledge, while undesirable data will be filtered.
\end{abstract}
\section{Introduction}
\labelsec{intro}
For many years, bacterial cells are considered primarily as selfish
individuals, but, in recent years, it has become evident that, far from
operating in isolation, they coordinate collective behavior in response to
environmental challenges using sophisticated inter cellular communication
networks. Cell-to-cell communication between bacteria is mediated by small
diffusible signal molecules that trigger changes in gene expression in response
to fluctuations in population density. This process, generally referred to as
quorum sensing (\gls{QS}). Recent advances have revealed that bacteria are not
limited to communication within their own species but are capable of \textbf{listening
in} and \textbf{broadcasting to} between also unrelated species (\emph{heterogeneous
system}). This \emph{behavior} allow them to intercept messages and coerce
cohabitants into behavioral modifications.
\newpage
\section{Background}
\labelsec{background}
In most environments, bacteria are found in mixed communities, and therefore
populations of a given \gls{QS} species that can sense a signal broadcast information
both to themselves and to other species sharing the same ecological niche
\emph{(Environ)}.\\
Different bacterial species may \emph{speak} the same \gls{QS} \emph{language}, some may
possess sensors for specific \gls{QS} signals that facilitate eavesdropping while
others may manipulate the \gls{QS} activities of neighboring bacteria by degrading
the \gls{QS} signal molecules within their locality. See
Figure~\ref{fig:imgs/bacteria-qs}.\\\newline
\begin{figure}[!h]
  \centering
  \includegraphics[width=0.8\linewidth]{imgs/bacteria-qs.jpg}
  \caption{Releasing molecules = Quorum Sensing \cite{tedtalkBonnie}}
  \label{fig:imgs/bacteria-qs}
\end{figure}
Bacteria are the most abundant and adaptive form of life on Earth and, as such,
have an impact on the habitats and lifestyles of all other living organisms. We
would therefore be naive to think that, over the millennia, coevolution of pro-
and eukaryotes has not stimulated the development of inter-kingdom signalling
pathways promoting parasitic, symbiotic or commensal relationships; the members
of each biological kingdom possess \emph{hormone-like} molecules responsible for
cell–cell communication, a fact that is fundamental to the concept that the
members of any given kingdom will respond to the signals produced by another.
See Figure~\ref{fig:imgs/bacteria-bound}.\\\newpage
\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.8\linewidth]{imgs/bacteria-bound.jpg}
  \caption{Communication Channels \cite{tedtalkBonnie}}
  \label{fig:imgs/bacteria-bound}
\end{figure}
Bacteria, in general, are capable of \textbf{coordinating} their behavior collectively.
\gls{QS} is now a well-established concept in bacteriology, is increasingly being
discovered in unicellular eukaryotic microbes and has even been used to describe
the behavior of ants \cite{pratt2002quorum}, metastatic cells and even
iron-impregnated particles \cite{stewart2009riddle}. There is
also the intriguing possibility that different species from one kingdom may
coordinate their behavior against the individual/s from another kingdom.
Irrespective of how the signals are processed, the results will either be a
collective change in behavior for the good of the community as a whole or will
favor one or more species over others.\\\newline For further information, Bonnie's talk at
\gls{TED} has more insights about \emph{"How bacteria 'talk'"}\cite{tedtalkBonnie}.
\section{Project Analysis}
\labelsec{prjanal}
The project goal is to reproduce/emulate bacteria behavior learned in previous
section. The simulation shall prove that using computational agents that
mimicking a nature inspired system could help us make progress on data-filtering problems
like for example, \gls{SPAM}. Before going deeper into the problem solution, I
will define some notions.\\\newline
We \cite{saomicini1516} define \emph{simulation} as:
\begin{itemize}
  \item "\textit{the imitation of the operation of a
real-world process or system over time}" \cite{parisi2001simulazioni}
  \item "\textit{Simulation is the process with which we can study the dynamic evolution of a model system, usually through computational tools}" \cite{banks1999introduction}
\end{itemize}
Simulation requires a \textbf{model}, a representation/abstraction of an actual
system that involves \textsc{aggregation, simplification} and
\textsc{omission}.\\\newline
Citing Siddhartha Mukherjee \cite{tedtalkSidd} \textit{"You know, when an architect builds a model, he or she is trying to show
you a world in miniature. But when a scientist is building a model, he or she
is trying to show you the world in metaphor. He or she is trying to create a
new way of seeing. The former is a scale shift. The latter is a perceptual
shift."}\\\newline
In our example simulation create a virtual laboratory where generation of an
artificial evolution in bacterial colonies came up. We need a model that should
be able to capture:
\begin{itemize}
  \item the presence of \textbf{autonomous entities} (\emph{bacterium}) whose
    actions and interactions determine the evolution of the system
  \item effects of local-global interaction, self-organization and emergence of
    proprieties related only to the whole system
\end{itemize}
We know that such a model exist and it is called \gls{ABM}. We \cite{saomicini1516} have
defined it as "\textit{a specific individual-based computational model for
  studying macro emergent phenomena through the definition of the system micro
  level which is modeled as a collection of interacting entities.}"\\\newline
Now we try to explain our building blocks in terms of:
\begin{itemize}
  \item Structure
  \item Interaction
  \item Behavior
\end{itemize}
\subsection{Structure}
Starting with the \textbf{environment}. It is composed by different connected
area. Each area has its own:
\begin{enumerate}
  \item $\alpha$ Bacteria
  \item $\beta$ bad data
  \item $\Gamma$ good data
  \item $\Delta$ other \emph{intra}-species molecules that help each bacteria
    filter \textit{bad} data
\end{enumerate}
\textit{Good} and \textit{bad} data are metaphors of nearby \gls{QS} molecules in real
life scenario. These molecules could influence bacteria lives.\\\newline
\textbf{Bacterium} as living system has a \emph{boundary} and an \textit{immune system}
wired in his genes. The boundary purpose is \cite{saomicini1516}:
\begin{itemize}
  \item set it apart from what it is not
  \item simultaneously regulate its interactions with the environment
\end{itemize}
Bacteria are not heteronymous because \emph{input} and \emph{output}
alone do not determine the system. Bacteria like all other living entities
establish their own cycles in \textbf{time}. \textit{Live, do something, die.}
\subsection{Interaction}
The interaction between bacteria is mediated by the environment. Each bacterium
releases molecules outside his boundary and it perceives \emph{intra}-species
and \emph{inter}-species ones with receptors. \textit{There is not point-to-point
communication between two bacteria.} In order to achieve data filtering goals
only \emph{intra}-species communication is important. These two channel
communication could lead into a \textbf{security mechanism} (one channel
could be work like a trusted one and the another not).
\subsection{Behavior}
\textbf{Bacterium} act like a \textit{"dumb"} organism that do only these things:
\begin{enumerate}
  \item Perceive other \emph{intra}-species bacteria in the area
  \item If the perceived number is greater than a threshold start \gls{QS}
    phase: \begin{itemize}
      \item Perceive molecules/\textbf{immune genes} from others
        \emph{intra}-species bacteria
      \item Release outside his boundary (into the \textit{environment}) its own molecules/\textbf{immune genes}
    \end{itemize}
  \item Else move to another nearby area
  \item Filter outside molecules that are not from trusted source (it could be
    \textsc{dangerous}). If an unknown molecule is "\textit{bad}" and bacterium
    han not immune gene that could destroy the molecule it will be damaged.
    Otherwise it decompose the molecule and other
    \emph{intra}-specie bacteria will benefit from this action (in the same environment).
\end{enumerate}

\textbf{Autonomy} characteristics captured into the whole system are:
\begin{itemize}
  \item Boundaries, each bacterium and environment area
  \item \gls{Homeostasis}, separating internal molecules knowledge from external
    ones
  \item Internalization, thanks to preceptors bacteria are able to internalize
    other bacteria knowledge (molecules) released into the environment
  \item Gain in size, in terms of \emph{immune genes} that are able to filter more
    \textit{bad} data
  \item Flexibility, bacteria prefer areas with high intra-species population
    density in order to make stromatolites but they could also survive alone
\end{itemize}
More information about autonomy and which resources to change it could be found in \cite{rosslenbroich2014origin}.

\section{Project Implementation}
\labelsec{prjimpl}
The implementation could be made using different technologies and frameworks. I
choose \tucson\ \cite{tucson} in order to achieve our goal for two reason:
\begin{itemize}
  \item It is a good model that can handle the problem without abstraction gaps
  \item We \cite{saomicini1516} have used it in previous activities and so I
    have working examples to begin with.
\end{itemize}
\subsection{Structure}
Before we begin with implementation details, I would like to say something about
the main directories and file in the root project:
\begin{itemize}
  \item \textit{README.rst}, a classical \textsc{README} that you can find in
    every \gls{VCS}
  \item \textit{bac}, source code root directory
  \item \textit{background}, science papers about bacteria, \gls{QS}, etc.
  \item \textit{paper}, \LaTeX{} root directory, with all files needed to generate
    \textmd{this} report
\end{itemize}
Now follow an in depth description of project's source-code \textbf{structure}.
Inside \textsc{bac} I have \textit{libs} directory with needed libraries (\textbf{tucson.jar} and
\textbf{2p.jar}) and \textit{src} directory with the package
\textsc{sa.mantani.tucson}. Inside the package:
\begin{itemize}
  \item \uppercase{bacteria}:
    \begin{itemize}
      \item \texttt{Bacteria}, release a configurable random number of bacterium
        in each \texttt{Area} (\textit{TupleCentre})
      \item \texttt{Bacterium}, \texttt{AbstractTucsonAgent} that mimic real
        life behavior discussed in previous sections \ref{sec:background}
        \ref{sec:prjanal}
      \item \texttt{Gene}, each bacterium has his own immune system that help
        him \textbf{filter} \textit{bad} data perceived in the
        \texttt{Environment}
      \item \texttt{ImmuneSystem}, set of genes in a bacterium
    \end{itemize}
  \item \uppercase{data}:
    \begin{itemize}
      \item \texttt{Data}, in order to decouple future filtering rules, it is an
        \textbf{Adapter}
      \item \texttt{DataInfo}, knowledge base for \texttt{Data}
    \end{itemize}
  \item \uppercase{env}:
    \begin{itemize}
      \item \texttt{Area}, class used by the bacterium that has informations
        about near areas (\textit{TupleCentre})
      \item \texttt{Environment}, init each \textit{TupleCentre} defined in the
        \textit{Topology} with
        \textit{good} and \textit{bad} data
    \end{itemize}
  \item \uppercase{gui}:
    \begin{itemize}
      \item \texttt{SwarmListener}, listener for \textit{button} press
        and \textit{timer} tick actions
      \item \texttt{SwarmMonitor}, \tucson\ \textit{TupleCentre} monitor to
        update the \gls{GUI}
      \item \texttt{bacteriaGUI}, base class to start the \gls{GUI}
      \item \texttt{bacteriaGUI.form}, \textit{XML} file with form meta-data
    \end{itemize}
  \item \uppercase{launchers}:
    \begin{itemize}
      \item \texttt{LaunchEnvironment}, self explain
      \item \texttt{LaunchGUI}, self explain
      \item \texttt{LaunchScenario}, start everything, \textbf{main}
      \item \texttt{LaunchTopology}, self explain
    \end{itemize}
  \item \uppercase{utils}:
    \begin{itemize}
      \item \texttt{Boot}, start \tucson\ Node
      \item \texttt{Installer}, init \tucson\ Node
      \item \texttt{Topology}, \textit{TupleCentres} topology configuration
    \end{itemize}
\end{itemize}
\subsection{Interaction}
All interaction process are governed by \tucson\ \textit{TupleCentres}. Each
\textit{TupleCentre} has a finite set of tuples. This implementation has the
following tuples' structure (\emph{Prolog} like):
\begin{itemize}
  \item Environment's Data, \texttt{data(N)} like \texttt{data(-15)} (\textit{bad})
    and \texttt{data(25)} (\textit{good})
  \item Bacterium, \texttt{bacterium(NAME,HEALTH)} like \texttt{bacterium(top0,42)}
  \item Near \textit{TupleCentres}, \texttt{nbr(TCID)} like
    \texttt{nbr('@'(center,':'(localhost,20504)))}
\end{itemize}
\subsection{Behavior}
Implemented application is a simulation program that let you tune parameters in
order to achieve different outputs. Configurable parameters will be inside a
configuration file in a future release, but at the moment you can find them inside the following class like
\textit{static final} variables:
\begin{itemize}
  \item Inside \texttt{DataInfo}:
    \begin{minted}{java}
public abstract class DataInfo {
 public static final int BAD_DATA_LIMIT = 25;
 public static final int GOOD_DATA_LIMIT = 25;
 public static final int BAD_DATA_EACH_AREA = 15;
 public static final int GOOD_DATA_EACH_AREA = 25;
 ...
}
    \end{minted}
  \item Inside \texttt{Bacterium}:
    \begin{minted}{java}
public class Bacterium extends AbstractTucsonAgent {
 private final static Long TIMEOUT = 500L;
 private final static int NGENES = 3;
 private final static int STROMATOLITE_THRESHOLD = 5;
 private final static int HEALTH = 50;
 private final static int MAX_CYCLES = 500;
 ...
}
    \end{minted}
  \item Inside \texttt{Bacterium}:
    \begin{minted}{java}
public class Bacterium extends AbstractTucsonAgent {
 private final static Long TIMEOUT = 500L;
 private final static int NGENES = 3;
 private final static int STROMATOLITE_THRESHOLD = 5;
 private final static int HEALTH = 50;
 private final static int MAX_CYCLES = 500;
 ...
}
    \end{minted}
  \item Inside \texttt{Bacteria}:
    \begin{minted}{java}
public final class Bacteria {

 private final static int BACTERIA_PER_AREA = 20;

 ...

}
    \end{minted}
  \item \texttt{Topology} is handle at runtime by the system, you could change
    it and always \textbf{should} work fine. \textbf{\uppercase{NB} \gls{GUI} is
    hardcoded only for Topology}:
\begin{minted}{java}
public final class Topology {
public static final List<Area> areas = new ArrayList<>(5);

public static void bootTopology() {
 try {
  Area center = new Area("center", "localhost", "20504");
  Area top = new Area("top", "localhost", "20512");
  Area right = new Area("right", "localhost", "20503");
  Area bottom = new Area("bottom", "localhost", "20506");
  Area left = new Area("left", "localhost", "20509");

	...
 }
}
\end{minted}
\end{itemize}
\section{Conclusion}
\labelsec{conclusion}
Running \texttt{LaunchScenario}, \gls{GUI} looks like~\ref{fig:screenshot1}.
\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.8\linewidth]{imgs/screenshot-gui2.jpg}
  \caption{Working Example Screenshot}
  \label{fig:screenshot1}
\end{figure}
In the screenshot, we can see in a poorly way how bacteria survive
better in team. Stromatolite that came up in \textit{center}
\textit{TupleCentre} has more \textbf{filter} rules that help bacteria inside
have better \textbf{health average}. This is an emergent behavior thanks
to \emph{intra}-species \gls{QS} bacteria interaction. This simulator could be
used and adapted in a more sophisticated \textbf{anti-spam} scenario, where
\textit{bad} data has not a simple structure like a negative integer number. It
is only required to adapt \texttt{Data} class.\\\newline

Last but no the least I would like to thank Andrea Omicini, Stefano Mariani and
all collaborators to \tucson\ eco-system, without whom this paper would be less
than few words of theory.
