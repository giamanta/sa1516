/**
 *
 */
package sa.mantani.tucson.launchers;

import java.util.logging.Level;
import java.util.logging.Logger;

import sa.mantani.tucson.bacteria.Bacteria;
import sa.mantani.tucson.gui.bacteriaGUI;
import sa.mantani.tucson.env.Environment;
import sa.mantani.tucson.utils.Topology;

/**
 * @author Giacomo
 */
public class LaunchScenario {

    public static void main(final String[] args) {

        Logger.getAnonymousLogger().log(Level.INFO, "Booting topology...");
        Topology.bootTopology();
        Logger.getAnonymousLogger().log(Level.INFO, "...topology boot");

        Environment.config();

        bacteriaGUI.main(args);

        Bacteria.release();

    }

}
