package sa.mantani.tucson.launchers;

import sa.mantani.tucson.gui.bacteriaGUI;

public final class LaunchGUI {

    public static void main(String[] args) {
        bacteriaGUI.main(args);
    }

}
