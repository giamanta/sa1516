package sa.mantani.tucson.bacteria;

import java.util.ArrayList;
import java.util.List;

public class ImmuneSystem {
    private List<Gene> genes;

    public ImmuneSystem(int n) {
        this.genes = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            this.genes.add(new Gene());
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Gene> getGenes() {
        return genes;
    }

    public void addGene(Gene g) {
        this.genes.add(g);
    }

}
