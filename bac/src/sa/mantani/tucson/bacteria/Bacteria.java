package sa.mantani.tucson.bacteria;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import sa.mantani.tucson.env.Area;
import sa.mantani.tucson.utils.Topology;

public final class Bacteria {

    private final static int BACTERIA_PER_AREA = 10;

    public static void release() {
        try {
            Random rand = new Random();
            rand.setSeed(System.currentTimeMillis());
            for (Area a : Topology.areas)
                for (int i = 0; i < Math.abs(rand.nextInt() % BACTERIA_PER_AREA) + 1; i++) {
                    Logger.getAnonymousLogger().log(Level.INFO,
                            "Releasing bacterium " + a.getName() + i + "...");
                    new Bacterium(a.getName() + i, a.getName(), a.getNetid(), Integer.parseInt(a.getPort())).go();
                    Logger.getAnonymousLogger().log(Level.INFO,
                            a.getName() + i + " released");
                }
        } catch (final TucsonInvalidAgentIdException e) {
            e.printStackTrace();
        }
    }

}
