package sa.mantani.tucson.bacteria;

import java.util.logging.Level;
import java.util.logging.Logger;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.EnhancedSynchACC;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.core.AbstractTupleCentreOperation;
import sa.mantani.tucson.data.Data;
import sa.mantani.tucson.data.DataInfo;

public class Bacterium extends AbstractTucsonAgent {

    private final static Long TIMEOUT = 500L;
    private final static int NGENES = 4;
    private final static int STROMATOLITE_THRESHOLD = 5;
    private final static int HEALTH = 50;
    private final static int MAX_CYCLES = 500;

    private TucsonTupleCentreId tcid;
    private EnhancedSynchACC acc;

    private boolean stopped;
    private final ImmuneSystem ims;
    private int health;

    public Bacterium(final String bid, final String tcName, final String netid, int port) throws TucsonInvalidAgentIdException {
        super(bid, netid, port);
        this.acc = null;
        this.ims = new ImmuneSystem(Bacterium.NGENES);
        this.health = Bacterium.HEALTH;
        try {
            this.tcid = new TucsonTupleCentreId(tcName, netid, "" + port);
        } catch (TucsonInvalidTupleCentreIdException e) {
            e.printStackTrace();
        }

        this.stopped = false;
    }

    @Override
    protected void main() {

        this.acc = this.getContext();

        this.init();

        Logger.getAnonymousLogger().log(Level.INFO, this.myName() + ") wakes up.");

        int cycles = 0;
        while (!this.stopped && this.health > 0 && cycles++ < Bacterium.MAX_CYCLES) {
            if (this.perceiveNColony() > Bacterium.STROMATOLITE_THRESHOLD) {
                this.quorumSensing();
            } else {
                // move to another area
                this.move(getNextArea());
            }
            this.filter();

            try {
                Thread.sleep(Bacterium.TIMEOUT);
            } catch (final InterruptedException e) {
                this.stopped = true;
            }

        }

        Logger.getAnonymousLogger().log(Level.INFO, this.myName() + ") Bye bye!");

    }

    private void init() {
        try {
            this.acc.out(this.tcid,
                    LogicTuple.parse(this.toTupleOutString()), null);
        } catch (InvalidLogicTupleException
                | TucsonOperationNotPossibleException
                | UnreachableNodeException | OperationTimeOutException e1) {
            this.err("Error while booting!");
        }
    }

    private void quorumSensing() {
        Logger.getAnonymousLogger().log(Level.INFO, this.myName() + ") Quorum Sensing procedure start.");

        this.perceiveMolecules();

        this.releaseImsMolecules();

        Logger.getAnonymousLogger().log(Level.INFO, this.myName() + ") Quorum Sensing procedure end.");
    }

    private void perceiveMolecules() {
        // bacterium perceive from the env immune system genes released by other intra-species bacteria
        Logger.getAnonymousLogger().log(Level.INFO, this.myName() + ") perceiving molecules...");

        ITucsonOperation op;
        try {
            op = this.acc.rdAll(this.tcid, LogicTuple.parse("immune(GENE)"), Bacterium.TIMEOUT);
            if (op.isResultSuccess())
                for (LogicTuple lt : op.getLogicTupleListResult())
                    this.ims.addGene(new Gene(LogicTuple.parse(lt.getArg(0).toString())));
            else
                this.err("Error while perceive molecules: no immune(GENE) found!");
        } catch (InvalidLogicTupleException
                | TucsonOperationNotPossibleException
                | UnreachableNodeException | OperationTimeOutException e1) {
            this.err("Error while perceive all molecules from env!");
        }
    }

    private void releaseImsMolecules() {
        // bacterium release into the env his immune system genes
        Logger.getAnonymousLogger().log(Level.INFO, this.myName() + ") releasing molecules...");
        try {
            for (Gene gene : this.ims.getGenes())
                this.acc.out(this.tcid,
                        LogicTuple.parse("immune(" + gene.getValue().toString() + ")"), Bacterium.TIMEOUT);
        } catch (InvalidLogicTupleException
                | TucsonOperationNotPossibleException
                | UnreachableNodeException | OperationTimeOutException e1) {
            this.err("Error while releasing molecules to env!");
        }
    }

    /*
        return the number of the bacteria in an area (# colony members)
     */
    private int perceiveNColony() {
        // bacterium perceive from the env immune system genes released by other intra-species bacteria
        Logger.getAnonymousLogger().log(Level.INFO, this.myName() + ") perceiving # colony members...");

        ITucsonOperation op;
        try {
            op = this.acc.rdAll(this.tcid, LogicTuple.parse("bacterium(NAME,HEALTH)"), Bacterium.TIMEOUT);
            return op.getLogicTupleListResult().size();
        } catch (InvalidLogicTupleException
                | TucsonOperationNotPossibleException
                | UnreachableNodeException | OperationTimeOutException e1) {
            this.err("Error while perceive # of bacteria in a colony (area) from env!");
        }

        return 0;
    }

    private LogicTuple getNextArea() {
        ITucsonOperation op = null;
        try {
            op = this.acc.urdp(this.tcid, LogicTuple.parse("nbr(NBR)"),
                    Bacterium.TIMEOUT);
        } catch (InvalidLogicTupleException
                | TucsonOperationNotPossibleException
                | UnreachableNodeException | OperationTimeOutException e) {
            this.err("Error while getting next area: "
                    + e.getClass().getSimpleName());
        }
        if (op != null && op.isResultSuccess()) {
            return op.getLogicTupleResult();
        }
        this.err("Error while getting next area: no nbrs found!");
        return null;
    }

    private void move(final LogicTuple direction) {

        ITucsonOperation op = null;

        try {
            op = this.acc
                    .uinp(this.tcid,
                            LogicTuple.parse(this.toTupleInString()),
                            Bacterium.TIMEOUT);
        } catch (InvalidLogicTupleException
                | TucsonOperationNotPossibleException
                | UnreachableNodeException | OperationTimeOutException e) {
            this.err("Error while moving [acc.uinp](1): " + e.getClass().getSimpleName());
        }

        if (op != null) {

            try {
                // LogicTuple.getArg(0).getArg(0) = name
                // LogicTuple.getArg(0).getArg(1).getArg(0) = netid (localhost)
                // LogicTuple.getArg(0).getArg(1).getArg(1) = port
                this.tcid = new TucsonTupleCentreId(direction.getArg(0)
                        .getArg(0).toString(), direction.getArg(0).getArg(1)
                        .getArg(0).toString(), direction.getArg(0).getArg(1)
                        .getArg(1).toString());
            } catch (final TucsonInvalidTupleCentreIdException e) {
                this.err("Error while moving [new TucsonTupleCentreId](2): "
                        + e.getClass().getSimpleName());
            }

            try {
                this.acc.out(this.tcid,
                        LogicTuple.parse(this.toTupleOutString()), null);
            } catch (InvalidLogicTupleException
                    | TucsonOperationNotPossibleException
                    | UnreachableNodeException | OperationTimeOutException e) {
                this.err("Error while moving [acc.out](3): "
                        + e.getClass().getSimpleName());
            }

        } else {
            this.err("Error while moving: cannot find myself!");
        }

    }

    private void filter() {
        ITucsonOperation op = null;

        try {
            op = this.acc.uinp(this.tcid,
                    LogicTuple.parse("data(N)"),
                    Bacterium.TIMEOUT);
        } catch (InvalidLogicTupleException
                | TucsonOperationNotPossibleException
                | UnreachableNodeException | OperationTimeOutException e) {
            this.err("Error while perceive data from env [acc.uinp](data(N)): " + e.getClass().getSimpleName());
        }
        if (op != null && op.isResultSuccess()) {
            // Now I should check N
            Data d = new Data(op.getLogicTupleArgument());
            if (DataInfo.isBadData(d)) { // antigen
                // Have I immune gene?
                boolean antibody = ims.getGenes().stream().anyMatch(gene -> gene.getValue().equals(d));

                if (!antibody) { // the antigen damage me, reject it to env!
                    this.health--;
                    try {
                        this.acc.out(this.tcid,
                                LogicTuple.parse(d.toString()), null);
                    } catch (InvalidLogicTupleException
                            | TucsonOperationNotPossibleException
                            | UnreachableNodeException | OperationTimeOutException e1) {
                        this.err("Error while booting!");
                    }
                }
            }
            // If good data nothing happen
        } else {
            this.log("No data in env, bad or good is up to you!");
        }

    }

    private String toTupleInString() {
        return "bacterium(" + this.myName() + ",HEALTH)";
    }

    private String toTupleOutString() {
        return "bacterium(" + this.myName() + "," + this.health + ")";
    }

    private void log(final String msg) {
        System.out.println("[" + this.toTupleInString() + "]: " + msg);
    }

    private void err(final String msg) {
        System.err.println("[" + this.toTupleInString() + "]:" + msg);
    }

    @Override
    public void operationCompleted(final ITucsonOperation op) {
        // Not used atm
    }

    @Override
    public void operationCompleted(final AbstractTupleCentreOperation arg0) {
        // Not used atm
    }

}
