package sa.mantani.tucson.bacteria;

import alice.logictuple.LogicTuple;
import sa.mantani.tucson.data.Data;

import java.util.IllegalFormatException;

public class Gene {
    private Data value;

    public Data getValue() {
        return value;
    }

    public Gene() {
        this.value = new Data();
        this.value.genBadData();
    }

    public Gene(LogicTuple lt) {
        try {
            this.value = new Data(lt);
        } catch (IllegalFormatException e) {
            System.err.println("[ERROR] Creation of a Gene from LogicTuple failed");
            System.err.println("        " + "Tried with:" + this.value.toString());
            System.err.println("[/ERROR]");
        }
    }
}
