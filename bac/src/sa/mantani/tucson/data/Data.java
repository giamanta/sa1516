package sa.mantani.tucson.data;

import alice.logictuple.LogicTuple;

import java.util.Random;

public class Data {
    private Integer content;
    private Random rand;

    public Data() {
        rand = new Random();
    }

    public Data(LogicTuple content) {
        this();
        this.content = content.getArg(0).intValue();
    }

    public Integer getContent() {
        return content;
    }

    public void genBadData() {
        this.content = genData(DataInfo.BAD_DATA_LIMIT) * -1;
    }

    public void genGoodData() {
        this.content = genData(DataInfo.GOOD_DATA_LIMIT);
    }

    private int genData(int limit) {
        rand.setSeed(System.currentTimeMillis());
        return Math.abs(rand.nextInt(limit)) + 1;
    }

    @Override
    public String toString() {
        return "data(" + content + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Data data = (Data) o;

        return !(content != null ? !content.equals(data.content) : data.content != null);

    }

    @Override
    public int hashCode() {
        return content != null ? content.hashCode() : 0;
    }
}
