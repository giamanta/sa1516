package sa.mantani.tucson.data;

import java.util.Random;

public abstract class DataInfo {
    public static final int BAD_DATA_LIMIT = 25;
    public static final int GOOD_DATA_LIMIT = 25;
    public static final int BAD_DATA_EACH_AREA = 15;
    public static final int GOOD_DATA_EACH_AREA = 25;

    public static boolean isBadData(Data d) {
        return Integer.signum(d.getContent().intValue()) == -1;
    }

    public static boolean isGoodData(Data d){
        return !isBadData(d);
    }

}
