package sa.mantani.tucson.env;

import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Area {
    private String name;
    private String netid;
    private String port;
    private List<Area> nearAreas;
    private TucsonTupleCentreId tcid;

    public Area(String name, String netid, String port) throws TucsonInvalidTupleCentreIdException {
        this.name = name;
        this.netid = netid;
        this.port = port;
        this.nearAreas = new ArrayList<>();
        this.tcid = new TucsonTupleCentreId(this.name, this.netid, this.port);
    }

    public String getNearAreasAsTucsonLogicTuple() {
        return "[" + this.nearAreas.stream()
                .map(area -> "nbr(" + area.toString() + ")")
                .collect(Collectors.joining(",")) + "]";
    }

    @Override
    public String toString() {
        return this.name + "@" + this.netid + ":" + this.port;
    }

    /* Getter & Setter  {{{ */
    public TucsonTupleCentreId getTcid() {
        return tcid;
    }

    public void setTcid(TucsonTupleCentreId tcid) {
        this.tcid = tcid;
    }

    public String getNetid() {
        return netid;
    }

    public void setNetid(String netid) {
        this.netid = netid;
    }

    public List<Area> getNearAreas() {
        return nearAreas;
    }

    public void setNearAreas(List<Area> nearAreas) {
        this.nearAreas = nearAreas;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /* Getter & Setter  }}} */
}
