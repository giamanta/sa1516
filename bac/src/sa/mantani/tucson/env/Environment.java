package sa.mantani.tucson.env;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.EnhancedSynchACC;
import alice.tucson.api.NegotiationACC;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonMetaACC;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import sa.mantani.tucson.data.Data;
import sa.mantani.tucson.data.DataInfo;
import sa.mantani.tucson.utils.Topology;

public final class Environment {

    private static TucsonAgentId me;
    private static NegotiationACC negAcc;
    private static EnhancedSynchACC acc;

    public static void config() {

        Environment.acquireACC();

        Environment.log("Configuring environment...");

        Topology.areas.stream().forEach(Environment::initArea);

        Environment.log("...environment configured");

    }

    private static void initArea(Area area) {
        try {
            Environment.log("Configuring area <" + area.getName() + ">...");
            Environment.acc
                    .outAll(area.getTcid(), LogicTuple.parse(area.getNearAreasAsTucsonLogicTuple()), null);
            Data d = new Data();
            for (int i = 0; i < DataInfo.BAD_DATA_EACH_AREA; i++) {
                d.genBadData();
                Environment.acc.out(area.getTcid(), LogicTuple.parse(d.toString()), null);
            }
            for (int i = 0; i < DataInfo.GOOD_DATA_EACH_AREA; i++) {
                d.genGoodData();
                Environment.acc.out(area.getTcid(), LogicTuple.parse(d.toString()), null);
            }
        } catch (final InvalidLogicTupleException e) {
            Environment.err("Invalid tuple given: " + e.getCause());
            System.exit(-1);
        } catch (final TucsonOperationNotPossibleException e) {
            Environment.err("Operation forbidden: " + e.getCause());
            System.exit(-1);
        } catch (final UnreachableNodeException e) {
            Environment.err("Cannot reach node: " + e.getCause());
            System.exit(-1);
        } catch (final OperationTimeOutException e) {
            Environment.err("Timeout exceeded: " + e.getCause());
            System.exit(-1);
        }
        Environment.log("...<" + area.getName() + "> configured");
    }

    private static void acquireACC() {
        try {
            Environment.me = new TucsonAgentId("env");
            Environment.negAcc = TucsonMetaACC
                    .getNegotiationContext(Environment.me);
            Environment.acc = Environment.negAcc.playDefaultRole();
        } catch (final TucsonInvalidAgentIdException e) {
            Environment.err("Invalid agent id given: " + e.getCause());
            System.exit(-1);
        } catch (final TucsonOperationNotPossibleException e) {
            Environment.err("Operation forbidden: " + e.getCause());
            System.exit(-1);
        } catch (final UnreachableNodeException e) {
            Environment.err("Cannot reach node: " + e.getCause());
            System.exit(-1);
        } catch (final OperationTimeOutException e) {
            Environment.err("Timeout exceeded: " + e.getCause());
            System.exit(-1);
        }
    }

    private static void log(final String msg) {
        System.out.println("[ENV]: " + msg);
    }

    private static void err(final String msg) {
        System.err.println("[ENV]: " + msg);
    }

}
