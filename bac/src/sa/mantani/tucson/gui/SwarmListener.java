package sa.mantani.tucson.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SwarmListener implements ActionListener {

    private final bacteriaGUI component;
    private SwarmMonitor monitor;

    public SwarmListener(final bacteriaGUI c) {
        this.component = c;
        this.monitor = null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(final ActionEvent e) {
        if (this.monitor == null) {
            this.monitor = new SwarmMonitor(this.component);
        }
        this.monitor.performUpdate();
    }

}
