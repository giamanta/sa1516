package sa.mantani.tucson.gui;

import javax.swing.*;

public class bacteriaGUI extends JComponent {
    private JButton btnUpdate;
    private JLabel lblBottom;
    private JPanel mainPanel;
    private JLabel lblLeft;
    private JLabel lblCenter;
    private JLabel lblRight;
    private JLabel lblTop;
    private Timer autoupdate;

    public JButton getBtnUpdate() {
        return btnUpdate;
    }

    public void setBtnUpdate(JButton btnUpdate) {
        this.btnUpdate = btnUpdate;
    }

    public JLabel getLblBottom() {
        return lblBottom;
    }

    public void setLblBottom(JLabel lblBottom) {
        this.lblBottom = lblBottom;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public void setMainPanel(JPanel mainPanel) {
        this.mainPanel = mainPanel;
    }

    public JLabel getLblLeft() {
        return lblLeft;
    }

    public void setLblLeft(JLabel lblLeft) {
        this.lblLeft = lblLeft;
    }

    public JLabel getLblRight() {
        return lblRight;
    }

    public void setLblRight(JLabel lblRight) {
        this.lblRight = lblRight;
    }

    public JLabel getLblCenter() {
        return lblCenter;
    }

    public void setLblCenter(JLabel lblCenter) {
        this.lblCenter = lblCenter;
    }

    public JLabel getLblTop() {
        return lblTop;
    }

    public void setLblTop(JLabel lblTop) {
        this.lblTop = lblTop;
    }

    public bacteriaGUI() {
        btnUpdate.addActionListener((e) -> toggleAutoupdate());
        this.autoupdate = new Timer(500, new SwarmListener(this));
        this.autoupdate.start();
    }

    public void toggleAutoupdate() {
        if (this.autoupdate.isRunning())
            this.autoupdate.stop();
        else
            this.autoupdate.start();
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("bacteriaGUI");
        frame.setContentPane(new bacteriaGUI().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(800,500);
        frame.setVisible(true);
    }

}
