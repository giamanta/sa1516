/**
 *
 */
package sa.mantani.tucson.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.swing.SwingUtilities;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.BulkSynchACC;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.NegotiationACC;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonMetaACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import sa.mantani.tucson.env.Area;
import sa.mantani.tucson.utils.Topology;

public class SwarmMonitor {

    final bacteriaGUI component;
    private BulkSynchACC acc;

    public SwarmMonitor(final bacteriaGUI c) {
        this.component = c;
        try {
            NegotiationACC negAcc = TucsonMetaACC
                    .getNegotiationContext(new TucsonAgentId("tcsMonitor"));
            this.acc = negAcc.playDefaultRole();
        } catch (TucsonInvalidAgentIdException
                | TucsonOperationNotPossibleException
                | UnreachableNodeException | OperationTimeOutException e) {
            e.printStackTrace();
        }
    }

    public void performUpdate() {
        Properties props;
        for (Area a : Topology.areas) {
            props = getBacteriaHealthAvg(monitor(a.getTcid()));
            switch (a.getName()) {
                case "center":
                    this.component.getLblCenter()
                            .setText("[# bacteria :" + props.getProperty("bacteria-num") + "] " +
                                    "[health AVG :" + props.getProperty("health-avg") + "]");
                    break;
                case "top":
                    this.component.getLblTop()
                            .setText("[# bacteria :" + props.getProperty("bacteria-num") + "] " +
                                    "[health AVG :" + props.getProperty("health-avg") + "]");
                    break;
                case "right":
                    this.component.getLblRight()
                            .setText("[# bacteria :" + props.getProperty("bacteria-num") + "] " +
                                    "[health AVG :" + props.getProperty("health-avg") + "]");
                    break;
                case "bottom":
                    this.component.getLblBottom()
                            .setText("[# bacteria :" + props.getProperty("bacteria-num") + "] " +
                                    "[health AVG :" + props.getProperty("health-avg") + "]");
                    break;
                case "left":
                    this.component.getLblLeft()
                            .setText("[# bacteria :" + props.getProperty("bacteria-num") + "] " +
                                    "[health AVG :" + props.getProperty("health-avg") + "]");
                    break;
                default:
                    SwarmMonitor.err("Unhandled GUI area in SwarmMonitor.performUpdate()!");
                    break;
            }
        }
        SwingUtilities.invokeLater(() -> {
            SwarmMonitor.this.component.revalidate();
            SwarmMonitor.this.component.repaint();
        });
    }

    private List<LogicTuple> monitor(TucsonTupleCentreId tcid) {
        try {
            ITucsonOperation op;
            SwarmMonitor.log("Monitor " + tcid.getName() + "...");
            op = this.acc.rdAll(tcid, LogicTuple.parse("bacterium(NAME,HEALTH)"), null);
            if (op.isResultSuccess())
                return op.getLogicTupleListResult();
            else
                SwarmMonitor.err("Error while monitoring for update: <rd_all> failure!");
        } catch (InvalidLogicTupleException
                | TucsonOperationNotPossibleException
                | UnreachableNodeException |
                OperationTimeOutException e
                ) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    private Properties getBacteriaHealthAvg(List<LogicTuple> lts) {
        Properties props = new Properties();
        int bacteria = 0, healthAvg = 0;
        for (LogicTuple lt : lts) {
            bacteria++;
            healthAvg += lt.getArg(1).intValue();
        }
        // Now in healthAvg we have the TOT, getting AVG
        healthAvg /= bacteria == 0 ? 1 : bacteria;
        props.put("bacteria-num", String.valueOf(bacteria));
        props.put("health-avg", String.valueOf(healthAvg));
        return props;
    }

    private static void log(final String msg) {
        System.out.println("[TcsMonitor]: " + msg);
    }

    private static void err(final String msg) {
        System.err.println(msg);
    }

}
