package sa.mantani.tucson.utils;

import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import sa.mantani.tucson.env.Area;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class Topology {
    public static final List<Area> areas = new ArrayList<>(5);

    public static void bootTopology() {
        try {
            Area center = new Area("center", "localhost", "20504");
            Area top = new Area("top", "localhost", "20512");
            Area right = new Area("right", "localhost", "20503");
            Area bottom = new Area("bottom", "localhost", "20506");
            Area left = new Area("left", "localhost", "20509");

            center.setNearAreas(Arrays.asList(top, right, bottom, left));
            Topology.areas.add(center);
            Boot.boot(center);

            top.setNearAreas(Arrays.asList(left, center, right));
            Topology.areas.add(top);
            Boot.boot(top);

            right.setNearAreas(Arrays.asList(top, center, bottom));
            Topology.areas.add(right);
            Boot.boot(right);

            bottom.setNearAreas(Arrays.asList(left, center, right));
            Topology.areas.add(bottom);
            Boot.boot(bottom);

            left.setNearAreas(Arrays.asList(top, center, bottom));
            Topology.areas.add(left);
            Boot.boot(left);
        }catch (TucsonInvalidTupleCentreIdException e){
            e.printStackTrace();
        }
    }

}
