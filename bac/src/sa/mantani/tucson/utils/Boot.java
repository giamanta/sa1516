/**
 *
 */
package sa.mantani.tucson.utils;

import alice.tucson.network.exceptions.DialogInitializationException;
import alice.tucson.service.TucsonNodeService;
import sa.mantani.tucson.env.Area;

/**
 * @author ste
 */
public class Boot {

    private final static int SLEEP_TIME = 1000;

    public static void boot(final Area area) {

        final int port = Boot.checkPort(area.getPort());

        Boot.log("Booting node on port " + port + "...");

    /* Boot TuCSoN node */
        TucsonNodeService node;
        node = new TucsonNodeService(port);
        new Installer(node).start();
        try {
            while (!TucsonNodeService.isInstalled(port, Boot.SLEEP_TIME)) {
                Thread.sleep(Boot.SLEEP_TIME);
            }
        } catch (final InterruptedException e) {
            Boot.err("Too early in the morning :Q___");
            System.exit(-1);
        } catch (final DialogInitializationException e) {
            Boot.err("Cannot connect to node on port " + port);
            System.exit(-1);
        }
        Boot.log("...node on port " + port + " boot");

    }

    private static int checkPort(final String p) {
    /* Check TuCSoN node port */
        int port = 0;
        try {
            port = Integer.parseInt(p);
        } catch (final NumberFormatException e) {
            Boot.err("No valid port number given");
            System.exit(-1);
        }
        if ((port < 1) || (port > 65535)) {
            Boot.err("No valid port number given");
            System.exit(-1);
        }
        // Boot.log("" + port);
        return port;
    }

    private static void err(final String msg) {
        System.err.println(msg);
    }

    private static void log(final String msg) {
        System.out.println(msg);
    }

}
