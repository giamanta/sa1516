##############################################
Data filtering model based on Self-Assembly and Quorum-Sensing in bacterial colonies
##############################################

.. topic:: Abstract

    Stromatolites are colonies of bacteria that self-assemble into rock
    formations in tidal salt flats. Each stromatolite can make independent
    decisions, while maintaining communication with the colony. The workload is
    shared among all colony individuals. Stromatolites breed rapidly, and
    quickly develop resistance to antibiotics and other threats by developing
    new genes. I plan to incorporate these principles into a simulated data
    filtering system. The system is composed by autonomous agents acting like
    bacteria. Workload i.e., incoming data, will be spread evenly through the
    network. And in a process mimicking natural selection, desirable data will
    be quickly distributed to incoming knowledge, while undesirable data will be
    filtered.

    :Date: 2015-12-02
    :Author: **Giacomo Mantani**
		:Matricola: 749068


.. contents::
    :depth: 3

More info
#########
./paper/

Project Plan
#############

* [OK] Template LaTeX, lei ha uno stile che preferisce usare per i progetti o ne uso uno mio?
* [OK] Tecnologia da utilizzare per la simulazione, pensavo di utilizzare TuCSoN. Lei cosa ne pensa?
* [OK] Creazione di un agente batterio
  + [OK] Definizione della conoscenza di base, struttura iniziale alla nascita
  + [OK] Definizione del comportamento evolutivo
  + [OK] Definizione dell' interazione con gli altri batteri
* [OK] Creazione GUI
* [OK] Risposte e conclusioni, il modello rappresentato si comporta similmente al sistema naturale?

Demo (INSTALL/RUN)
##################

Run `LaunchScenario` and alway works. (**NB** there should be `tucson.jar` and
`2p.jar` in java classpath)

.. rubric:: Bibliography
.. [ask-nature] http://www.asknature.org/strategy/ccce7e2ef8c3f16da4211ccdebf94469
.. [royal] http://rsif.royalsocietypublishing.org/content/12/113/20150724
.. [qs] http://www.nottingham.ac.uk/quorum/nottingham.htm
.. [citeseerx] http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.105.1125
.. [article] http://pubs.rsc.org/en/content/articlelanding/2009/sm/b812146j
.. [book-self] Self Assembly: The Science of Things That Put Themselves Together